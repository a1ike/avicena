$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.a-modal').toggle();
  });

  $('.a-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-modal__centered') {
      $('.a-modal').hide();
    }
  });

  $('.a-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').hide();
  });

  $('.open-info').on('click', function (e) {
    e.preventDefault();

    $('.a-info').toggle();
  });

  $('.a-info__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-info__centered') {
      $('.a-info').hide();
    }
  });

  $('.a-info__close').on('click', function (e) {
    e.preventDefault();
    $('.a-info').hide();
  });

  $('.a-header__dropper').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('a-header__dropper_active');
    $('.a-header__drop').slideToggle('fast');
  });

  $('.a-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.a-header__main').slideToggle('fast');
  });

  $('.a-home-seo__more').on('click', function (e) {
    e.preventDefault();

    if ($(this).hasClass('a-home-seo__more_active')) {
      $('.a-home-seo__more > span').html('Читать дальше');
      $('.a-home-seo__content').toggleClass('a-home-seo__content_active');
      $('.a-home-seo__more').toggleClass('a-home-seo__more_active');
    } else {
      $('.a-home-seo__more > span').html('Скрыть');
      $('.a-home-seo__content').toggleClass('a-home-seo__content_active');
      $('.a-home-seo__more').toggleClass('a-home-seo__more_active');
    }
  });

  $('.a-home-seo__more_unactive').on('click', function (e) {
    e.preventDefault();
    $('.a-home-seo__more > span').html('Скрыть');
    $('.a-home-seo__content').addClass('a-home-seo__content_active');
    $(this).removeClass('a-home-seo__more_unactive');
    $(this).addClass('a-home-seo__more_active');
  });

  $('.a-contacts-card').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('a-contacts-card_active');
    $(this).find('.a-contacts-card__title').next().slideToggle('fast');
  });

  $('.a-footer__toggle').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('a-footer__toggle_active');
    $(this).next().slideToggle('fast');
  });

  $('.a-service__toggle_services').on('click', function (e) {
    e.preventDefault();

    if ($('#serviceServices').is(':visible')) {
      $('#serviceServices').slideToggle('fast');
      return;
    }
    $('.a-service__content').hide();
    $('#serviceServices').slideToggle('fast');
  });

  $('.a-service__toggle_form').on('click', function (e) {
    e.preventDefault();

    if ($('#serviceForm').is(':visible')) {
      $('#serviceForm').slideToggle('fast');
      return;
    }
    $('.a-service__content').hide();
    $('#serviceForm').slideToggle('fast');
  });

  $('.a-info-collapse__header').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('a-info-collapse__header_active')
    $(this).next().slideToggle('fast');
  });

  $(window).on('load resize', function (e) {

    if ($(window).width() < 1200) {
      let newContent = $('.a-header__menu li').clone();
      $('.a-header__main').append(newContent);
      $('.a-header__menu').remove();

      $('.a-service__replace').parent().detach().prependTo($('.gallery-top'));
    }
  });

  new Swiper('.a-home-top', {
    loop: false,
    navigation: {
      nextEl: '.a-home-top .swiper-button-next',
      prevEl: '.a-home-top .swiper-button-prev',
    },
    pagination: {
      el: '.a-home-top .swiper-pagination'
    },
  });

  new Swiper('#homeWhere', {
    loop: true,
    autoplay: {
      delay: 4000,
    },
    navigation: {
      nextEl: '#homeWhere .swiper-button-next',
      prevEl: '#homeWhere .swiper-button-prev',
    },
  });

  new Swiper('#homeWhy', {
    loop: true,
    autoplay: {
      delay: 4000,
    },
    navigation: {
      nextEl: '#homeWhy .swiper-button-next',
      prevEl: '#homeWhy .swiper-button-prev',
    },
  });

  new Swiper('.a-home-doctors__cards', {
    loop: true,
    freeMode: false,
    slidesPerView: 1,
    spaceBetween: 30,
    scrollbar: {
      el: '.a-home-doctors__cards .swiper-scrollbar',
      draggable: true,
      dragSize: 27,
    },
    /* pagination: {
      el: '.a-home-doctors__cards .swiper-pagination',
      clickable: false,
    }, */
    breakpoints: {
      768: {
        slidesPerView: 3,
        spaceBetween: 60,
      },
      1200: {
        slidesPerView: 5,
        spaceBetween: 60,
        freeMode: true,
        scrollbar: {
          snapOnRelease: false,
        },
      },
    },
    progressbarOpposite: true,
  });

  new Swiper('.a-home-reviews__cards', {
    slidesPerView: 1,
    spaceBetween: 30,
    pagination: {
      el: '.a-home-reviews__cards .swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.a-home-reviews__cards .swiper-button-next',
      prevEl: '.a-home-reviews__cards .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
    }
  });

  var galleryThumbs = new Swiper('.gallery-thumbs', {
    loop: false,
    allowTouchMove: false,
    navigation: {
      nextEl: '.gallery-thumbs .swiper-button-next',
      prevEl: '.gallery-thumbs .swiper-button-prev',
    },
    spaceBetween: 10,
    slidesPerView: 'auto',
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      1200: {
        slidesPerView: 'auto',
        spaceBetween: 40,
        loop: true,
        allowTouchMove: true,
      },
    }
  });
  var galleryTop = new Swiper('.gallery-top', {
    loop: false,
    allowTouchMove: false,
    spaceBetween: 10,
    navigation: {
      nextEl: '.gallery-thumbs .swiper-button-next',
      prevEl: '.gallery-thumbs .swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs,
    },
    breakpoints: {
      1200: {
        loop: true,
        allowTouchMove: true,
        loopedSlides: 4,
      },
    }
  });
});
